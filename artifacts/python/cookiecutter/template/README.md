# Cookiecutter Python Project

This template was inspired by [claws/cookiecutter-python-project](https://github.com/claws/cookiecutter-python-project)

For information visit [cookiecutter's readthedocs](https://cookiecutter.readthedocs.io/en/1.7.2/first_steps.html)
