{{cookiecutter.package_display_name}}
{{cookiecutter.package_display_name|length * '#' }}

{{cookiecutter.package_short_description}}

.. _user-guild-label:

User Guide & Quick Start
========================

The `User Guide <https://construction-zone.gitlab.io/bedrock/python/{{cookiecutter.package_name}}/user/>`_ provides a quick start & install guide.

.. _api-reference-label:

API Reference
=============

The `API Reference <https://construction-zone.gitlab.io/bedrock/python/{{cookiecutter.package_name}}/api/>`_ provides API-level documentation.
