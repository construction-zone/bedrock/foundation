"""{{cookiecutter.package_short_description}}"""

import importlib.metadata

__version__ = importlib.metadata.version('{{cookiecutter.package_name}}')
