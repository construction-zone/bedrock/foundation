User Guide
##########

This section of the documentation provides user focused information such as
installing and quickly using this package.

.. _quick-start-label:

Quick Start
===========

{{cookiecutter.package_display_name}} is available on PyPI and can be installed with `pip <https://pip.pypa.io>`_.

.. code-block:: console

    $ pip install {{cookiecutter.package_name}}

After installing {{cookiecutter.package_display_name}} you can use it like any other Python module.

Here is a simple example:

.. code-block:: python

    import {{cookiecutter.package_name}}
    # Fill this section in with the common use-case.

.. _install-guide-label:

Install Guide
=============

.. note::

    It is best practice to install Python projects in a virtual environment,
    which can be created and activated as follows using Python 3.6+.

    .. code-block:: console

        $ make python/venv
        $ source venv/bin/activate
        (myvenv) $

The simplest way to install {{cookiecutter.package_display_name}} is using Pip.

.. code-block:: console

    $ pip install {{cookiecutter.package_name}}

This will install ``{{cookiecutter.package_name}}`` and all of its dependencies.
