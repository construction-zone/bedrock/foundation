## Build the base images containing Makefile artifacts
docker/build-foundation:
	@cd ../../../ && docker build --target foundation -t foundation:artifacts .
	@cd ../../../ && docker build --target python -t foundation:python .
	@cd ../../../ && docker build --target venv -t foundation:venv .

## Build the a docker image containing the current python package
docker/build: docker/build-foundation
	$(call artifact-required,Dockerfile)
	$(call assert-set,DOCKER_IMAGE)
	$(call assert-set,DOCKER_TAG)
	$(call assert-set,DOCKER_TARGET)
	$(call assert-set,DOCKER_WORKDIR)
	$(call assert-set,DOCKER_ENTRYPOINT)
	@docker build --target ${DOCKER_TARGET} \
		--build-arg DOCKER_WORKDIR=${DOCKER_WORKDIR} \
		--build-arg DOCKER_ENTRYPOINT=${DOCKER_ENTRYPOINT} \
		-t ${DOCKER_IMAGE}:${DOCKER_TAG} .
	@docker system prune -f > /dev/null 2>&1
	
## Build w/o rebuilding foundation images (skips docker/build-foundation)
docker/build-fast:
	$(call artifact-required,Dockerfile)
	$(call assert-set,DOCKER_IMAGE)
	$(call assert-set,DOCKER_TAG)
	$(call assert-set,DOCKER_TARGET)
	$(call assert-set,DOCKER_WORKDIR)
	$(call assert-set,DOCKER_ENTRYPOINT)
	@docker build --target ${DOCKER_TARGET} \
		--build-arg DOCKER_WORKDIR=${DOCKER_WORKDIR} \
		--build-arg DOCKER_ENTRYPOINT=${DOCKER_ENTRYPOINT} \
		-t ${DOCKER_IMAGE}:${DOCKER_TAG} .
	@docker system prune -f > /dev/null 2>&1

## Build a docker image running debugpy
docker/build-debug-image:
	$(call assert-set,DOCKER_IMAGE)
	$(call assert-set,DOCKER_WORKDIR)
	$(call assert-set,DOCKER_ENTRYPOINT)
	@docker build --target debugpy \
		--build-arg DOCKER_WORKDIR=${DOCKER_WORKDIR} \
		--build-arg DOCKER_ENTRYPOINT=${DOCKER_ENTRYPOINT} \
		-t ${DOCKER_IMAGE}:debugpy .
	@docker system prune -f > /dev/null 2>&1

## Launch the docker container
docker/run: 
	$(call assert-set,PYTHON_PACKAGE)
	$(call assert-set,DOCKER_IMAGE)
	$(call assert-set,DOCKER_TAG)
	@docker run -it --rm \
		-v ${PWD}/src/${PYTHON_PACKAGE}:${DOCKER_ENTRYPOINT} \
		${DOCKER_IMAGE}:${DOCKER_TAG}

## Launches the docker container on a custom entrypoint            
docker/run-entrypoint:
	$(call assert-set,DOCKER_ENTRYPOINT)
	$(call assert-set,DOCKER_IMAGE)
	$(call assert-set,DOCKER_TAG)
	$(call assert-set,PYTHON_PACKAGE)
	@docker run -it --rm \
		-v ${PWD}/src/${PYTHON_PACKAGE}:${DOCKER_ENTRYPOINT} \
		--entrypoint ${DOCKER_ENTRYPOINT} \
		-it ${DOCKER_IMAGE}:${DOCKER_TAG}

## Launches the docker container in debug mode (listening on port 5678)
docker/run-debug:
	$(call assert-set,DOCKER_ENTRYPOINT)
	$(call assert-set,PYTHON_PACKAGE)
	$(call assert-set,DOCKER_IMAGE)
	@echo "debugpy started and listening on 0.0.0.0:5678"
	docker run -it --rm \
		-p 5678:5678 \
		-v ${PWD}/src/${PYTHON_PACKAGE}:${DOCKER_ENTRYPOINT} \
		${DOCKER_IMAGE}:debugpy

## Docker prune
docker/system-prune:
	@docker system prune -f

## Run  docker-compose up detached
docker-compose/up:
	$(call artifact-required,Dockerfile)
	$(call artifact-required,docker-compose.yml)
	@docker-compose up -d

## Run docker-compose down
docker-compose/down:
	$(call artifact-required,Dockerfile)
	$(call artifact-required,docker-compose.yml)
	@docker-compose down
