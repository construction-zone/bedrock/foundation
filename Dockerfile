## build a python base image
FROM gitlab.com/construction-zone/bedrock/dependency_proxy/containers/ubuntu:20.04 as python
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y -q \   
    && apt-get upgrade -y -q \
    && apt-get install -y -q \
        python3.9 \
        python3.9-venv \
    && apt-get clean \
    && apt autoremove -y \
    && rm -rf /var/lib/apt/lists/* 

## build image containing the required foundation artifacts
FROM gitlab.com/construction-zone/bedrock/dependency_proxy/containers/ubuntu:20.04 as foundation
WORKDIR /foundation
COPY artifacts artifacts
COPY packages/python/backhoe packages/python/backhoe
COPY packages/python/utility_belt packages/python/utility_belt

## build a dev venv
FROM foundation:python as venv
ENV APPDIR /foundation
WORKDIR ${APPDIR}/packages/python/backhoe
RUN apt-get update -yq \
    && apt-get install -yq \
        python3-pip \
        git
COPY --from=foundation:artifacts ${APPDIR} ${APPDIR}
RUN make python/clean python/install-dev